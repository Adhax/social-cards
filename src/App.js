import  React, { Component } from 'react';
import $ from 'jquery';
import moment from 'moment';
import Card from './Card';
import './App.css';


class App extends Component {

	constructor(props) {
    super(props);
    this.state = { 
			fData:[],
			iData:[],
			tData:[]
		 };
  }


	componentDidMount(){
		$.get('./fData.json', (result)=> this.setState({fData: result}));
		$.get('./iData.json', (result)=> this.setState({iData: result}));
		$.get('./tData.json', (result)=> this.setState({tData: result}));
	}

	render() {
		let fData = this.state.fData.map((item, index)=>{
			return(
				<Card
					key={index}
					img={this.state.fData[index].img}
					post={this.state.fData[index].post}
					date={this.state.fData[index].date}
					auther={"By " + this.state.fData[index].userId}				
				/>
			);
		});

		let iData = this.state.iData.map((item, index)=>{
			let t = moment(this.state.iData[index].timePosted, 'x');
			
			let formatted = t.toISOString().slice(0, 10);
			return(
				<Card
					key={index}
					img={this.state.iData[index].post}
					post={this.state.iData[index].descriptionText}
					date={formatted}
					url={"https://"+this.state.iData[index].url}
					goTo={(link) => window.location = link}	
				/>
			);
		});

		let tData = this.state.tData.map((item, index)=>{
			let t = moment(this.state.tData[index].date, 'YYYYMMDDTHHmmssZ');
			let formatted = t.toISOString().slice(0, 10);
			return(
				<Card
					key={index}
					img="https://upload.wikimedia.org/wikipedia/de/9/9f/Twitter_bird_logo_2012.svg"
					post={this.state.tData[index].status}
					date={formatted}
					auther={"By " + this.state.tData[index].username}				
		
				/>
			);
		});

		return (
			<div className="app">
				<h2>Facebook</h2>
				<div className="wrapper">
					{fData}
				</div>
				<h2>Instagram</h2>
				<div className="wrapper">
					{iData}
				</div>
				<h2>Twitter</h2>
				<div className="wrapper">
					{tData}
				</div>
			</div>
		);
	}
}

export default App;
