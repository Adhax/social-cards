import  React, { Component } from 'react';

class Card extends Component {

	render(){
		return(
			<div className="card"
			onClick={this.props.url ? ()=>this.props.goTo(this.props.url):null}
			>
				<img src={this.props.img} alt=""/>
				{this.props.post}
				<small className="date">
				{this.props.date}
				</small>
				<small className="author">
				{this.props.auther}
				</small>
			</div>
		);
	}
}

export default Card;
